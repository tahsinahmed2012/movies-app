import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class MovieService {
  constructor(private http: HttpClient) {}

  apiUrl = environment.apiUrl;
  apiKey = environment.apiKey;
  accountID = environment.accountId;
  sessionID = environment.sessionId;

  upcoming_movies_index(page: number): Observable<any> {
    return this.http.get(
      `${this.apiUrl}/movie/upcoming?api_key=${this.apiKey}&page=${page}`
    );
  }

  favorite_movies_index(page: number): Observable<any> {
    return this.http.get(
      `${this.apiUrl}/account/${this.accountID}/favorite/movies?api_key=${this.apiKey}&session_id=${this.sessionID}&sort_by=created_at.asc&page=${page}`
    );
  }

  movie_detail(id: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/movie/${id}?api_key=${this.apiKey}`);
  }

  favorite_movie(mediaDetails: any) {
    return this.http.post(
      `${this.apiUrl}/account/${this.accountID}/favorite?api_key=${this.apiKey}&session_id=${this.sessionID}`,
      mediaDetails
    );
  }
}
