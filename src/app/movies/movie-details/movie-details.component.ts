import { Component, OnInit } from "@angular/core";
import { MovieService } from "../service/movie.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs";

@Component({
  selector: "app-movie-details",
  templateUrl: "./movie-details.component.html",
  styleUrls: ["./movie-details.component.scss"]
})
export class MovieDetailsComponent implements OnInit {
  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  id: number;
  movie: Observable<any>;
  itemBackdropUrl = "https://image.tmdb.org/t/p/w1400_and_h450_face/";

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
      this.loadMovieDetail();
    });
  }

  loadMovieDetail() {
    this.movieService.movie_detail(this.id).subscribe(
      data => {
        this.movie = data;
        this.itemBackdropUrl += data.backdrop_path;
      },
      error => {
        console.log(error);
      }
    );
  }
}
