import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {MovieDetailsComponent} from './movie-details/movie-details.component';
import {FavoritesComponent} from './favorites/favorites.component';

const routes: Routes = [
  {
    path: 'home/:page',
    component: HomeComponent,
  },
  {
    path: 'movie/:id',
    component: MovieDetailsComponent,
  },
  {
    path: 'favorites/:page',
    component: FavoritesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MoviesRoutingModule { }
