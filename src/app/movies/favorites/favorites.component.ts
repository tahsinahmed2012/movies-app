import { Component, OnInit } from "@angular/core";
import { MovieService } from "../service/movie.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Observable } from "rxjs";

@Component({
  selector: "app-favorites",
  templateUrl: "./favorites.component.html",
  styleUrls: ["./favorites.component.css"]
})
export class FavoritesComponent implements OnInit {
  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  movies: Observable<any>[];
  page: number;
  totalPages: number;
  totalPageArray: any[] = [];

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.page = +params.page;
      this.loadMovies();
    });
  }

  loadMovies() {
    this.movieService.favorite_movies_index(this.page).subscribe(
      data => {
        this.movies = data.results;
        this.totalPages = data.total_pages;
        for (let i = 0; i < this.totalPageArray.length; i++) {
          this.totalPageArray[i] = i + 1;
        }
        for (const movie of this.movies) {
          // @ts-ignore
          if (movie.overview.length > 200) {
            // @ts-ignore
            movie.overview = movie.overview.substr(0, 200).concat("...");
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
