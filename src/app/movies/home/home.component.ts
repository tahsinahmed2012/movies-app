import { Component, OnInit } from "@angular/core";
import { MovieService } from "../service/movie.service";
import { Observable } from "rxjs";
import { ActivatedRoute, Params, Router } from "@angular/router";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  movies: Observable<any>[];
  page: number;
  totalPages: number;
  totalPageArray: any[] = [];

  mediaDetails = {
    media_type: "",
    media_id: 0,
    favorite: false
  };

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.page = +params.page;
      this.loadMovies();
    });
  }

  loadMovies() {
    this.movieService.upcoming_movies_index(this.page).subscribe(
      data => {
        this.movies = data.results;
        this.totalPages = data.total_pages;
        this.totalPageArray[this.totalPages - 1] = this.totalPages;
        for (let i = 0; i < this.totalPageArray.length; i++) {
          this.totalPageArray[i] = i + 1;
        }
        for (const movie of this.movies) {
          // @ts-ignore
          if (movie.overview.length > 200) {
            // @ts-ignore
            movie.overview = movie.overview.substr(0, 200).concat("...");
          }
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  favoriteMovie(movieId) {
    // @ts-ignore
    const favoriteMovie = this.movies.find(movie => movie.id === movieId);

    // @ts-ignore
    favoriteMovie.favorite = true;

    this.mediaDetails = {
      media_type: "movie",
      media_id: movieId,
      favorite: true
    };
    this.movieService.favorite_movie(this.mediaDetails).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
  }
}
