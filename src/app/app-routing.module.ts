import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SharedLayoutComponent} from './shared/shared-layout/shared-layout.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home/1'
  },
  {
    path: '',
    component: SharedLayoutComponent,
    children: [
      { path: '', loadChildren: '../app/movies/movies.module#MoviesModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
