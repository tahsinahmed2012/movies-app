import { Component, OnInit } from "@angular/core";
import { SharedServiceService } from "../service/shared-service.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  constructor(private sharedService: SharedServiceService) {}

  suggestions: Observable<any>[] = [];
  query: any;

  ngOnInit() {}

  search() {
    if (this.query) {
      this.sharedService.movie_search(this.query).subscribe(
        data => {
          this.suggestions = data.results;
          for (const suggestion of this.suggestions) {
            // @ts-ignore
            if (suggestion.title.length > 30) {
              // @ts-ignore
              suggestion.title = suggestion.title.substr(0, 30).concat("...");
            }
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }
}
