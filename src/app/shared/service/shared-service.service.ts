import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import {Observable} from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class SharedServiceService {
  constructor(private http: HttpClient) {}

  apiUrl = environment.apiUrl;
  apiKey = environment.apiKey;
  accountID = environment.accountId;
  sessionID = environment.sessionId;

  movie_search(query): Observable<any> {
    return this.http.get(
      `${this.apiUrl}/search/movie?api_key=${this.apiKey}&query=${query}&page=1&include_adult=false`
    );
  }
}
