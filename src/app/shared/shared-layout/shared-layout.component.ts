import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shared-layout',
  template: `
    <app-loader></app-loader>
    <app-header></app-header>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./shared-layout.component.css']
})
export class SharedLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
